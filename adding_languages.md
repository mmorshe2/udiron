# So you want to add a language to Udiron...

You've come to the right place!

This document serves as a sort of tutorial for those wishing to write language-specific machinery for Udiron.

(Throughout this tutorial it is assumed that you have udiron set up on your system, and thus its dependencies—particularly tfsl—also set up on your system. The example language used here is Bokmål, although the author of this tutorial might have misconstrued some elements of Bokmål grammar, so that what is actually authored for Bokmål better deals with things than this, and perhaps also uses Bokmål names for functions and variables and parameters.)

## 1. Pick a language object.

If you're lucky, then your desired language code-Wikidata item pair exists in `[tfsl.languages.langs](https://bitbucket.org/mmorshe2/tfsl/src/main/tfsl/languages.py#lines-74)` already, and it should suffice to import and use that for the rest of this tutorial:

```python
from tfsl.languages import langs
myLanguage = langs.nb_
```

Should no such pair exist at the moment (and assuming such pair is not otherwise eligible for inclusion in `tfsl.languages.langs`), you can always create one yourself:

```python
from tfsl.languages import Language
myLanguage = Language('nb', 'Q25167')
```

## 2. Model syntactic relationships.

Now that you have a language object defined, you can start considering how to model syntactic relations in your language.
For this tutorial, we'll consider something simple: the attachment of adjectives to nouns in Bokmål.

### 2.1. Find out how this relationship might be modeled in a dependency framework.

If your language is lucky enough to have a Universal Dependencies (UD) treebank, then you might find how sentences in it are modeled useful for determining how the functions you write construct them.
The list of languages with existing treebanks can be found [on the UD main page](https://universaldependencies.org/), and some interfaces to search these treebanks are maintained by the [Charles University of Prague](https://lindat.mff.cuni.cz/services/kontext/corpora/corplist) and [INRIA](http://match.grew.fr/).

If your languages does not have such a treebank, you might still take a look at the treebanks available for other languages, but also might benefit from consulting [the definitions of dependency relations](https://universaldependencies.org/u/dep/all.html) directly.

With respect to the adjective-noun relationship in Bokmål, let's consider the adjective [nedre](https://www.wikidata.org/wiki/Lexeme:L494369) and the noun [del](https://www.wikidata.org/wiki/Lexeme:L449156), which are connected in at least one sentence in a Bokmål treebank.
The adjective is a child to the left of the noun, using the 'amod' (adjectival modifier) dependency relationship.

### 2.2. Write a function to generate this relationship.

Once you've identified how this relationship might be modeled, you can go ahead and write a function that generates it. This example, while making a lot of assumptions about Bokmål grammar here, is otherwise something fully functional:

```python
from udiron.relations import rels

def addAdjectiveToNoun(noun_clause, adjective_clause):
    new_noun_clause = noun_clause.addChild(adjective_clause, rels.amod_, '(')
    return new_noun_clause
```

Note that this function (and all other language-specific machinery in Udiron) should only manipulate Clause objects—see udiron/clause.py for the methods available to you—although probing the lexemes in Clause objects is entirely okay.

### 2.3. (Rinse and repeat!)

Surely the adjective-noun relationship isn't the only one that can be expressed for Bokmål, so you should go ahead and write more functions to express more relationships, refactoring and revising existing functions along the way!

Here are some other possible functions that are also making lots of assumptions about Bokmål grammar:

```python
def makeNounDefinite(noun_clause):
    return noun_clause.addInflection('Q53997851')

def makeNounSingular(noun_clause):
    return noun_clause.addInflection('Q110786')
```

## 3. Write your language's string generation methods.

Now that you have at least one function for use in generating syntax trees, let's make sure that when we stringify it, we get a proper string out.

```python
from udiron.langs.mul.str import *

def selectForm_nb(lexeme_in, inflections_in):
    candidate_forms = lexeme_in.getForms(inflections_in)
    if(len(candidate_forms) == 0):
        if(lexeme_in.category == 'Q34698'): # adjective
            obtained_form = lexeme_in.getForms(['Q14169499'])[0] # comparative
        else:
            obtained_form = LexemeForm("[]" @ myLanguage)
    else:
        obtained_form = candidate_forms[0]
    current_form = next(rep.text for rep in obtained_form.representations if rep.language == myLanguage)
    return current_form

def processConfig_nb(config, form_in):
    return form_in

def surfaceJoin_nb(tokens_in):
    return surfaceJoin_spaces(tokens_in)

def surfaceTransform_nb(string_in):
    return string_in
```

## 4. Register your language's string generation methods.

*(This need only be written once; you are free to modify any of the methods later and simply reimport the library to have the changes take effect.)*

Once you have written these four string generation methods, you need to ensure that Udiron uses them when you stringify a Clause whose root language is Bokmål.
As a result you must register those methods with Udiron before Udiron can use them:

```python
from udiron.langs import StrMethods

StrMethods.registerSelectForm(selectForm_nb, myLanguage)
StrMethods.registerProcessConfig(processConfig_nb, myLanguage)
StrMethods.registerSurfaceJoin(surfaceJoin_nb, myLanguage)
StrMethods.registerSurfaceTransform(surfaceTransform_nb, myLanguage)
```

## 5. Try it out!

Now that the string generation methods are registered, we can try out our new Bokmål-specific machinery:

```python
from udiron import Clause
from udiron.utils import L

nedre_clause, del_clause = Clause(L(494369)), Clause(L(449156))
del_clause = makeNounDefinite(del_clause)
del_clause = makeNounSingular(del_clause)
del_clause = addAdjectiveToNoun(del_clause, nedre_clause)
print(str(del_clause)) # should get "nedre delen" as output
```

## Appendix: adding this machinery to Udiron itself

More on this is forthcoming.
