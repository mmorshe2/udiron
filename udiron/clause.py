from operator import itemgetter
from textwrap import indent

from sortedcontainers import SortedKeyList

from tfsl import L

import udiron.output

clause_list_key = lambda x: x[0].index
DEFAULT_INDENT = '    '
DEFAULT_PARENT_REL = 'Q1757074'  # root node

# TODO: if significant performance lag manifests, consider refactor without SortedKeyList
class Clause:
    def __init__(self, lexeme, language=None, sense=None, inflections=None, config=None):
        self.lexeme = lexeme
        self.index = 0
        self.left_index = 0
        self.right_index = 0
        self.parent = None
        self.parent_rel = DEFAULT_PARENT_REL
        self.children = SortedKeyList(key=clause_list_key)

        self.language = lexeme.language if language is None else language
        self.sense = lexeme.get_senses()[0] if sense is None else sense
        self.inflections = set() if inflections is None else set(inflections)
        self.config = {} if config is None else config

    def _add_child(self, clause_in, rel_in):
        clause_in.parent_rel = rel_in
        clause_in.parent = self
        self.children.add((clause_in, rel_in))
        return self

    def _substitute_child(self, index, shift):
        (clause, rel) = self.children[index]
        del self.children[index]
        clause.shift_indices(shift)
        self.children.add((clause, rel))

    def shift_higher_levels_attach_left(self, child_index, self_index, shift_width):
        for index in range(0, child_index):
            self._substitute_child(index, -shift_width)
        # check overlap with child to right in left subtree, if such a child is present
        current_right_index = self.children[child_index][0].right_index
        if child_index + 1 != self_index:
            following_left_index = self.children[child_index+1][0].left_index
            if following_left_index <= current_right_index:
                adjusted_shift = following_left_index - current_right_index - 1
                self._substitute_child(child_index, adjusted_shift)
        # check overlap with index position
        elif child_index + 1 == self_index:
            if self_index <= current_right_index:
                self._substitute_child(child_index, self.index - current_right_index - 1)
        self.left_index += -shift_width

    def shift_higher_levels_attach_right(self, child_index, self_index, shift_width):
        for index in range(len(self.children)-1, child_index-1, -1):
            self._substitute_child(index, shift_width)
        # check overlap with child to left in right subtree, if such a child is present
        current_left_index = self.children[child_index][0].left_index
        if child_index - 1 >= self_index:
            preceding_right_index = self.children[child_index-1][0].right_index
            if current_left_index <= preceding_right_index:
                adjusted_shift = current_left_index - preceding_right_index + 1
                self._substitute_child(child_index, adjusted_shift)
        # check overlap with index position
        elif child_index - 1 < self_index:
            if self.index > current_left_index:
                self._substitute_child(child_index, current_left_index - self.index + 1)
        self.right_index += shift_width

    # TODO: concoct better tests for these and fix
    def shift_higher_levels_attach(self, child, shift_width):
        child_index = self.children.index(child)
        self_index = self.children.bisect_key_left(self.index)
        if child_index < self_index:
            self.shift_higher_levels_attach_left(child_index, self_index, shift_width)
        else:
            self.shift_higher_levels_attach_right(child_index, self_index, shift_width)
        if self.parent is not None:
            self.parent.shift_higher_levels_attach((self, self.parent_rel), shift_width)

    def shift_higher_levels_detach_left(self, child_index, self_index, shift_width):
        # check gap with child to right in left subtree, if such a child is present
        current_right_index = self.children[child_index][0].right_index
        if child_index + 1 != self_index:
            following_left_index = self.children[child_index+1][0].left_index
            if following_left_index - current_right_index > 1:
                self._substitute_child(child_index, shift_width)
        # check gap with index position
        elif child_index + 1 == self_index:
            if self_index - current_right_index > 1:
                self._substitute_child(child_index, shift_width)
        for index in range(child_index-1, -1, -1):
            self._substitute_child(index, shift_width)
        self.left_index += shift_width

    def shift_higher_levels_detach_right(self, child_index, self_index, shift_width):
        # check gap with child to left in right subtree, if such a child is present
        current_left_index = self.children[child_index][0].left_index
        if child_index - 1 >= self_index:
            preceding_right_index = self.children[child_index-1][0].right_index
            if current_left_index - preceding_right_index > 1:
                self._substitute_child(child_index, -shift_width)
        # check gap with index position
        elif child_index - 1 < self_index:
            if self.index - current_left_index > 1:
                self._substitute_child(child_index, -shift_width)
        for index in range(child_index+1, len(self.children)):
            self._substitute_child(index, -shift_width)
        self.right_index += -shift_width

    # TODO: concoct better tests for these and fix
    def shift_higher_levels_detach(self, child, shift_width):
        child_index = self.children.index(child)
        self_index = self.children.bisect_key_left(self.index)
        if child_index < self_index:
            self.shift_higher_levels_detach_left(child_index, self_index, shift_width)
        else:
            self.shift_higher_levels_detach_right(child_index, self_index, shift_width)
        if self.parent is not None:
            self.parent.shift_higher_levels_detach((self, self.parent_rel), shift_width)

    def attach_left_of_root(self, clause_in, rel_in):
        end_point = self.children.bisect_key_left(self.index)
        clause_shift_width = -(clause_in.right_half_width + 1) + self.index - clause_in.index
        other_shift_width = -(clause_in.index_width + 1)
        # there are no left children
        if end_point == 0:
            clause_in.shift_indices(clause_shift_width)
            self.left_index += other_shift_width
        # there are left children
        else:
            clause_in.shift_indices(clause_shift_width)
            for i in range(0, end_point):
                self._substitute_child(i, other_shift_width)
            self.left_index += other_shift_width
        self._add_child(clause_in, rel_in)
        if self.parent is not None:
            self.parent.shift_higher_levels_attach((self, self.parent_rel), abs(other_shift_width))
        return self

    def attach_right_of_root(self, clause_in, rel_in):
        start_point = self.children.bisect_key_right(self.index)
        clause_shift_width = clause_in.left_half_width + 1 + self.index - clause_in.index
        other_shift_width = clause_in.index_width + 1
        # there are no right children
        if start_point == len(self.children):
            clause_in.shift_indices(clause_shift_width)
            self.right_index += other_shift_width
        # there are right children
        else:
            clause_in.shift_indices(clause_shift_width)
            for i in range(len(self.children)-1,start_point-1,-1):
                self._substitute_child(i, other_shift_width)
            self.right_index += other_shift_width
        self._add_child(clause_in, rel_in)
        if self.parent is not None:
            self.parent.shift_higher_levels_attach((self, self.parent_rel), abs(other_shift_width))
        return self

    def attach_leftmost(self, clause_in, rel_in):
        end_point = self.children.bisect_key_left(self.index)
        # there are no right children
        if end_point == 0:
            return self.attach_left_of_root(clause_in, rel_in)
        else:
            clause_shift_width = -(clause_in.right_half_width + 1 - self.left_index)
            clause_in.shift_indices(clause_shift_width - clause_in.index)
            self.left_index -= clause_in.index_width + 1
            return self._add_child(clause_in, rel_in)

    def attach_rightmost(self, clause_in, rel_in):
        start_point = self.children.bisect_key_right(self.index)
        # there are no right children
        if start_point == len(self.children):
            return self.attach_right_of_root(clause_in, rel_in)
        else:
            clause_shift_width = clause_in.left_half_width + 1 + self.right_index
            clause_in.shift_indices(clause_shift_width - clause_in.index)
            self.right_index += clause_in.index_width + 1
            return self._add_child(clause_in, rel_in)

    def attach_after_rel(self, after_rel, clause_in, rel_in):
        (match_list_index, _) = next((index, child)
            for (index, child) in enumerate(self.children)
            if child[1] == after_rel)
        return self.attach_after(match_list_index, clause_in, rel_in)

    def attach_after(self, match_list_index, clause_in, rel_in):
        match_child = self.children[match_list_index]
        match_child_index = clause_list_key(match_child)
        # add to the left subtree
        if match_child_index < self.index:
            clause_shift_width = -(clause_in.right_half_width - match_child[0].right_index)
            other_shift_width = -(clause_in.index_width + 1)
            for i in range(0, match_list_index+1):
                self._substitute_child(i, other_shift_width)
            clause_in.shift_indices(clause_shift_width)
            self.left_index += other_shift_width
        # add to the right subtree
        else:
            clause_shift_width = clause_in.left_half_width + match_child[0].right_index + 1
            other_shift_width = clause_in.index_width + 1
            for i in range(len(self.children)-1, match_list_index, -1):
                self._substitute_child(i, other_shift_width)
            clause_in.shift_indices(clause_shift_width)
            self.right_index += other_shift_width
        return self._add_child(clause_in, rel_in)

    def attach_before_rel(self, before_rel, clause_in, rel_in):
        (match_list_index, _) = next((index, child)
            for (index, child) in enumerate(self.children)
            if child[1] == before_rel)
        return self.attach_before(match_list_index, clause_in, rel_in)

    def attach_before(self, match_list_index, clause_in, rel_in):
        match_child = self.children[match_list_index]
        match_child_index = clause_list_key(match_child)
        # add to the left subtree
        if match_child_index < self.index:
            clause_shift_width = -(clause_in.right_half_width - match_child[0].left_index + 1)
            other_shift_width = -(clause_in.index_width + 1)
            for i in range(0, match_list_index):
                self._substitute_child(i, other_shift_width)
            clause_in.shift_indices(clause_shift_width)
            self.left_index += other_shift_width
        # add to the right subtree
        else:
            clause_shift_width = clause_in.left_half_width + match_child[0].left_index
            other_shift_width = clause_in.index_width + 1
            for i in range(len(self.children)-1, match_list_index-1, -1):
                self._substitute_child(i, other_shift_width)
            clause_in.shift_indices(clause_shift_width)
            self.right_index += other_shift_width
        return self._add_child(clause_in, rel_in)

    def detach(self, clause, rel):
        detached_index = self.children.index((clause, rel))
        if clause_list_key(self.children[detached_index]) > self.index:
            return self.detach_right_of_root(detached_index)
        else:
            return self.detach_left_of_root(detached_index)

    def detach_right_of_root(self, detached_index):
        detached_width = -(self.children[detached_index][0].index_width + 1)
        for index in range(detached_index,len(self.children)):
            self._substitute_child(index, detached_width)
        popped_child = self.children.pop(detached_index)
        self.right_index += detached_width
        if self.parent is not None:
            self.parent.shift_higher_levels_detach((self, self.parent_rel), abs(detached_width))
        return popped_child

    def detach_left_of_root(self, detached_index):
        detached_width = self.children[detached_index][0].index_width + 1
        popped_child = self.children.pop(detached_index)
        for index in range(detached_index-1,-1,-1):
            self._substitute_child(index, detached_width)
        self.left_index += detached_width
        if self.parent is not None:
            self.parent.shift_higher_levels_detach((self, self.parent_rel), abs(detached_width))
        return popped_child

    def shift_indices(self, shift_width):
        self.index += shift_width
        self.left_index += shift_width
        self.right_index += shift_width
        new_child_list = SortedKeyList(key=clause_list_key)
        for child in self.children:
            (clause, rel) = child
            clause.shift_indices(shift_width)
            new_child_list.add((clause, rel))
        self.children = new_child_list
        return self

    def get_left_children(self):
        end_point = self.children.bisect_key_left(self.index)
        return self.children[:end_point]

    def get_right_children(self):
        start_point = self.children.bisect_key_right(self.index)
        return self.children[start_point:]

    def add_inflection(self, inflection):
        self.inflections.add(inflection)
        return self

    def add_inflections(self, inflections):
        self.inflections.update(inflections)
        return self

    def remove_inflection(self, inflection):
        self.inflections.discard(inflection)
        return self

    def remove_inflections(self, inflections):
        self.inflections.difference_update(inflections)
        return self

    def get_current_form(self):
        return udiron.output.select_form(self.lexeme, self.inflections, self.language, self.config)

    def get_tokens(self):
        current_form = self.get_current_form()
        current_position = self.index
        token_list = [(current_form, current_position)]
        for (child, _) in self.children:
            token_list.extend(child.get_tokens())
        return token_list

    def get_child_from_rel_left(self, rel_in):
        same_rel = lambda child: child[1] == rel_in
        return next(filter(same_rel, self.children))

    def get_child_from_rel_right(self, rel_in):
        same_rel = lambda child: child[1] == rel_in
        return next(filter(same_rel, reversed(self.children)))

    # TODO: rework later
    def has_rel(self, rel_in):
        return any(child[1] == rel_in for (index, child) in enumerate(self.children))

    # TODO: rework with above later
    def get_rel(self, rel_in):
        return next(child for (index, child) in enumerate(self.children) if child[1] == rel_in)

    # TODO: rework with above later
    def detach_rel(self, rel):
        (clause, rel) = self.get_rel(rel)
        return self.detach(clause, rel)

    def pop(self):
        components = self.lexeme['P5238']
        if len(components) == 0:
            return self

        clause_objects = SortedKeyList(key=itemgetter(0))
        for component in components:
            current_lexeme = L(component.value.id)
            index = int(component.qualifiers['P1545'][0].value)
            if object_form := component.qualifiers.get("P5548", None):
                object_inflections = current_lexeme[object_form[0].value.id].features
            else:
                object_inflections = None
            try:
                object_sense = component.qualifiers['P5980'][0].value
            except (IndexError, KeyError):
                object_sense = None
            syntactic_parent = int(component.qualifiers['P9764'][0].value)
            syntactic_relationship = component.qualifiers['P9763'][0].value.id
            new_object = Clause(current_lexeme, sense=object_sense, inflections=object_inflections)
            new_object.shift_indices(index)
            clause_objects.add((index, new_object, syntactic_parent, syntactic_relationship))

        for (index, new_object, syntactic_parent, syntactic_relationship) in clause_objects:
            if syntactic_parent != 0:
                parent_index = clause_objects.bisect_key_left(syntactic_parent)
                new_parent = clause_objects[parent_index][1]
                new_object.parent = new_parent
                new_object.parent_rel = syntactic_relationship
                new_parent.children.add((new_object, syntactic_relationship))
            else:
                new_clause = new_object

        for (_, new_object, _, _) in clause_objects:
            new_object.children = SortedKeyList(new_object.children, key=clause_list_key)

        new_clause = rectify(new_clause)
        if(new_clause.index != 0):
            new_clause = new_clause.shift_indices(-new_clause.index)
        return new_clause

    def __str__(self):
        reordered = sorted(self.get_tokens(), key=itemgetter(1))
        # TODO: consider allowing config in these two steps
        output_string = udiron.output.surface_join(reordered, self.language)
        output_string = udiron.output.surface_transform(output_string, self.language)
        return output_string

    @property
    def index_range(self):
        return (self.left_index, self.right_index)

    @property
    def index_width(self):
        return self.right_index - self.left_index

    @property
    def left_half_width(self):
        return self.index - self.left_index

    @property
    def right_half_width(self):
        return self.right_index - self.index

    def __repr__(self):
        main_index_location = self.children.bisect_key_left(self.index)
        stmts_str_left = ""
        if main_index_location != 0:
            representations_l = [repr(child) for (child, _) in self.children[:main_index_location]]
            joined_representations_l = '\n'.join(representations_l)
            stmts_str_left = indent(joined_representations_l, DEFAULT_INDENT)+'\n'
        index_str = f'{self.index} [{self.left_index}, {self.right_index}]'
        base_str = f'{self.lexeme.lemmata} ' + index_str + f', {self.parent_rel}'
        stmts_str_right = ""
        if main_index_location != len(self.children):
            representations_r = [repr(child) for (child, _) in self.children[main_index_location:]]
            joined_representations_r = '\n'.join(representations_r)
            stmts_str_right = '\n'+indent(joined_representations_r, DEFAULT_INDENT)
        return stmts_str_left + base_str + stmts_str_right

# TODO: handle discontiguities if they exist
def rectify(clause_in):
    # if there are no children
    if len(clause_in.children) == 0:
        return clause_in
    for index in range(len(clause_in.children)):
        (clause, rel) = clause_in.children[index]
        del clause_in.children[index]
        clause = rectify(clause)
        clause_in.children.add((clause, rel))
        clause_in.left_index = min(clause_in.left_index, clause.left_index)
        clause_in.right_index = max(clause_in.right_index, clause.right_index)
    return clause_in