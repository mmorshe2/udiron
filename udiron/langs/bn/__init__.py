from tfsl import L

from udiron.clause import Clause
from udiron.langs.mul import *
import udiron.constants as C
import udiron.langs.bn.str

tam_mappings = {
    (C.present, None, C.indicative): C.simple_present,
    (C.present, C.continuous, C.indicative): C.continuous_present,
    (C.present, C.perfect_aspect, C.indicative): C.perfect_present,
    (C.past, None, C.indicative): C.simple_past,
    (C.past, C.continuous, C.indicative): C.continuous_past,
    (C.past, C.perfect_aspect, C.indicative): C.pluperfect,
    (C.past, C.habitual, C.indicative): C.habitual_past,
    (C.future, None, C.indicative): C.simple_future,
    (C.present, None, C.imperative): C.present_imperative,
    (C.future, None, C.imperative): C.future_imperative
}

def add_adjectival_modifier(clause_in, adj_modifier, **others):
    return clause_in.attach_left_of_root(adj_modifier, C.adj_attribute)

def add_nominal_subject(clause_in, subject, **others):
    subject = subject.add_inflection(C.nominative)
    return clause_in.attach_leftmost(subject, C.subject)

def add_nominal_object(clause_in, object_in, **others):
    if clause_in.has_rel(C.light_verb_construction):
        return clause_in.attach_before_rel(C.light_verb_construction, object_in, C.direct_object)
    return clause_in.attach_left_of_root(object_in, C.direct_object)

def add_copular_subject(clause_in, subject, **others):
    if 'tense' in others:
        if others['tense']['main'] == 'past':
            verb_clause = Clause(L(582310), inflections=[C.simple_past, C.third_person])
            clause_in = add_nominal_object(verb_clause, clause_in)
        elif others['tense']['main'] == 'future':
            verb_clause = Clause(L(582310), inflections=[C.simple_future, C.third_person])
            clause_in = add_nominal_object(verb_clause, clause_in)
    clause_in = add_nominal_subject(clause_in, subject)
    return clause_in

def get_existence_verb(tense):
    # TODO: keep as little in here as possible
    if tense is None:
        return Clause(L(582310))
    # TODO: handle aspect and other distinctions
    if tense['main'] in ('past', 'present'):
        tense_marker = C.simple_past if tense['main'] == 'past' else C.simple_present
        return Clause(L(582310), inflections=[tense_marker, C.third_person])
    else:
        return Clause(L(345845), inflections=[C.simple_future, C.third_person])

# TODO: adjust these to item mappings and search graph?
refine_tense_dict = {
    'hesternal': 588776,
    'hodiernal': 587438,
    'crastinal': 588777
}

def note_existence(subject, **others):
    acha_clause = get_existence_verb(others.get('tense', None))
    clause_in = add_nominal_subject(acha_clause, subject)
    if tense_dict := others.get('tense', False):
        if refine_tense := tense_dict.get('refine', False):
            time_modifier = Clause(L(refine_tense_dict[refine_tense]))
            clause_in.attach_leftmost(time_modifier, C.time_complement)
    return clause_in

def add_singular_classifier(clause_in, **others):
    return clause_in.attach_rightmost(Clause(L(295106), config={"compound": ['left']}), C.classifier)

def add_plural_classifier(clause_in, **others):
    return clause_in.attach_rightmost(Clause(L(295107), config={"compound": ['left']}), C.classifier)

def add_noun_count(clause_in, **others):
    # TODO: clean up somehow
    if 'definite' in others:
        if 'number' in others and others['number'] in ['plural', 'paucal']:
            clause_in = add_plural_classifier(clause_in)
        else:
            clause_in = add_singular_classifier(clause_in)
    else:
        if 'number' in others:
            classifier = Clause(L(303264)) # TODO: add a function to construct numbers
        else:
            classifier = Clause(L(303225))
        classifier.attach_rightmost(Clause(L(295106), config={"compound": ['left']}), C.classifier)
        clause_in = clause_in.attach_leftmost(classifier, C.numeral_adjective)
    return clause_in

def make_adjective_superlative(clause_in, **others):
    if any(C.superlative in form.features for form in clause_in.lexeme.get_forms()):
        clause_in = clause_in.add_inflection(C.superlative)
    else:
        degree_clause = Clause(L(584433))
        degree_clause.attach_right_of_root(Clause(L(584432), config={"compound": ['left']}), C.subordinate_conjunction)
        clause_in = clause_in.attach_left_of_root(degree_clause, C.mode_complement)
    return clause_in

def mark_locative_case(clause_in, **others):
    return clause_in.add_inflection(C.locative)

def add_location_modifier(clause_in, location_modifier, **others):
    return clause_in.attach_leftmost(location_modifier, C.location_complement)

def negate_clause(clause_in, **others):
    # TODO: actually implement this
    if clause_in.lexeme.lexeme_id == 'L582310' and C.simple_present in clause_in.inflections:
        clause_in = clause_in.add_inflection(C.negation)
    else:
        clause_in = clause_in.attach_rightmost(Clause(L(582560)), C.negation)
    return clause_in

def attach_demonstrative_determiner(clause_in, demonstrative, **others):
    determiner_mappings = {
        'proximal': [476061, 476052],
        'medial': [476064, 476053],
        'distal': [476066, 476056]
    }
    mapping_subindex = 1 if 'emphasize' in others and others['emphasize'] else 0
    determiner_clause = Clause(L(determiner_mappings[demonstrative][mapping_subindex]))
    constructor_clause = clause_in.attach_leftmost(determiner_clause, C.determiner)
    return constructor_clause

def inflect_for_tense(clause_in, **others):
    if 'tense' in others:
        if others['tense']['main'] == 'past':
            clause_in.add_inflection(C.simple_past)
        elif others['tense']['main'] == 'future':
            clause_in.add_inflection(C.simple_future)
        elif others['tense']['main'] == 'present':
            clause_in.add_inflection(C.simple_present)
        if refine_tense := others['tense'].get('refine', False):
            time_modifier = Clause(L(refine_tense_dict[refine_tense]))
            clause_in.attach_leftmost(time_modifier, C.time_complement)
    return clause_in

def inflect_for_person(clause_in, subject_in):
    if subject_in.lexeme.category in [C.pronoun, C.personal_pronoun]:
        # TODO: separately store the lexeme ID as a number
        if subject_in.lexeme.lexeme_id in ["L60055", "L252191"]:
            inflection = C.first_person
        elif subject_in.lexeme.lexeme_id in ["L60057", "L252193"]:
            inflection = C.informal_second_person
        elif subject_in.lexeme.lexeme_id in ["L60056", "L252192"]:
            inflection = C.general_second_person
        elif subject_in.lexeme.lexeme_id in ["L60058", "L252194"]:
            inflection = C.bengali_polite_form
        else:
            inflection = C.third_person
    else:
        inflection = C.third_person
    return clause_in.add_inflection(inflection)

def generate_speaker(**scope):
    if "number" in scope and scope["number"] == "plural":
        output_clause = Clause(L(252191)) # আমরা
    else:
        output_clause = Clause(L(60055)) # আমি
    return output_clause

listener_pronouns = {
    ("singular", "informal"): 60057,
    ("plural", "informal"): 252193,
    ("singular", "familiar"): 60056,
    ("plural", "familiar"): 252192,
    ("singular", "formal"): 60058,
    ("plural", "formal"): 252194
}

def generate_listener(**scope):
    if "number" not in scope:
        number = "singular"
    else:
        number = scope["number"]
    if "formality" not in scope:
        formality = "formal"
    else:
        formality = scope["formality"]
    output_clause = Clause(L(listener_pronouns[(number, formality)]))
    return output_clause

def add_thematic_inflections(clause_in, grammatical_feature_reqs):
    if clause_in.has_rel(C.classifier):
        (classifier_clause, classifier_rel) = clause_in.get_rel(C.classifier)
        classifier_clause.add_inflections(grammatical_feature_reqs)
        return clause_in
    return clause_in.add_inflections(grammatical_feature_reqs)