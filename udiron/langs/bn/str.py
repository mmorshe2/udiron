import random

from tfsl import langs, LexemeForm

from udiron.langs import StrMethods
from udiron.langs.mul.str import surface_join_spaces, surface_transform_rmspaces
import udiron.constants as C

rep_language_is_bn = lambda rep: rep.language == langs.bn_

def develop_form_bn_adjective(lexeme_in, inflections_in, config_in):
    return lexeme_in.get_forms([C.positive])[0]

def develop_form_bn_noun(lexeme_in, inflections_in, config_in):
    return lexeme_in.get_forms([C.nominative])[0]

def develop_form_bn_verb(lexeme_in, inflections_in, config_in):
    if C.negation in inflections_in: # only L582310 has forms with negation on them
        return lexeme_in.get_forms([C.negation])[0]
    present_tense = any(x in inflections_in for x in [C.simple_present, C.present_imperative])
    sadhu_or_chalit = any(x in inflections_in for x in [C.chalit_bhasa, C.sadhu_bhasa])
    if(present_tense and sadhu_or_chalit):
        inflection_set = inflections_in - set([C.chalit_bhasa, C.sadhu_bhasa])
    else:
        inflection_set = [C.verbal_noun]
    return lexeme_in.get_forms(inflection_set)[0]

def develop_form_bn(lexeme_in, inflections_in, config_in):
    if lexeme_in.category == C.adjective:
        return develop_form_bn_adjective(lexeme_in, inflections_in, config_in)
    elif lexeme_in.category in [C.noun, C.proper_noun]:
        return develop_form_bn_noun(lexeme_in, inflections_in, config_in)
    elif lexeme_in.category == C.verb:
        return develop_form_bn_verb(lexeme_in, inflections_in, config_in)
    elif lexeme_in.category == C.classifier:
        obtained_form = random.choice(lexeme_in.get_forms([C.nominative]))
    elif lexeme_in.category == C.numeral:
        obtained_form = random.choice(lexeme_in.get_forms())
    else:
        try:
            obtained_form = next(iter(lexeme_in.get_forms()))
        except StopIteration:
            obtained_form = LexemeForm("[]" @ langs.bn_)
    return obtained_form

def handle_compound_bn(current_form, compound_setting):
    if current_form[-1] == '-':
        current_form = current_form[:-1]
    if current_form[0] == '-':
        current_form = current_form[1:]
    if "left" in compound_setting:
        current_form = C.COMPOUND_SEP + current_form
    if "right" in compound_setting:
        current_form = current_form + C.COMPOUND_SEP
    return current_form

def select_form_bn(lexeme_in, inflections_in, config_in):
    if len(inflections_in) == 0:
        obtained_form = develop_form_bn(lexeme_in, inflections_in, config_in)
    else:
        candidate_forms = lexeme_in.get_forms(inflections_in)
        if len(candidate_forms) == 0:
            obtained_form = develop_form_bn(lexeme_in, inflections_in, config_in)
        else:
            obtained_form = candidate_forms[0]
    current_form = next(filter(rep_language_is_bn, obtained_form.representations))
    current_form = current_form.text
    if "compound" in config_in:
        current_form = handle_compound_bn(current_form, config_in["compound"])
    return current_form


def surface_join_bn(tokens_in):
    return surface_join_spaces(tokens_in)


def surface_transform_bn(string_in):
    return surface_transform_rmspaces(string_in)


StrMethods.register_select_form(select_form_bn, langs.bn_)
StrMethods.register_surface_join(surface_join_bn, langs.bn_)
StrMethods.register_surface_transform(surface_transform_bn, langs.bn_)
