from collections import defaultdict


class StrMethods:
    __selectform__ = defaultdict()
    __surfacejoin__ = defaultdict()
    __surfacetransform__ = defaultdict()

    @classmethod
    def register_select_form(cls, method_in, language_in):
        cls.__selectform__[language_in] = method_in

    @classmethod
    def get_select_form(cls, language_in):
        return cls.__selectform__[language_in]

    @classmethod
    def register_surface_join(cls, method_in, language_in):
        cls.__surfacejoin__[language_in] = method_in

    @classmethod
    def get_surface_join(cls, language_in):
        return cls.__surfacejoin__[language_in]

    @classmethod
    def register_surface_transform(cls, method_in, language_in):
        cls.__surfacetransform__[language_in] = method_in

    @classmethod
    def get_surface_transform(cls, language_in):
        return cls.__surfacetransform__[language_in]
