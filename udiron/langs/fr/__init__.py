from tfsl import L

from udiron.clause import Clause
from udiron.langs.mul import *
import udiron.constants as C
import udiron.langs.fr.str

def inflect_for_number(clause_in, subject_in):
    if subject_in.lexeme.lexeme_id in ["L9094", "L9096"]:
        clause_in.add_inflection(C.singular)
    elif subject_in.lexeme.lexeme_id in ["L9288", "L9289"]:
        clause_in.add_inflection(C.plural)
    return clause_in

def inflect_for_person(clause_in, subject_in):
    if subject_in.lexeme.category in [C.pronoun, C.personal_pronoun]:
        # TODO: separately store the lexeme ID as a number
        if subject_in.lexeme.lexeme_id in ["L9288", "L9094"]:
            inflection = C.first_person
        elif subject_in.lexeme.lexeme_id in ["L9096", "L9289"]:
            inflection = C.second_person
        else:
            inflection = C.third_person
    else:
        inflection = C.third_person
    return clause_in.add_inflection(inflection)

def inflect_for_tense(clause_in, **others):
    # TODO: adjust add_copular_subject to use this
    # TODO: handle aspect and other distinctions
    refine_tense_dict = {
        'hesternal': 10742,
        'hodiernal': 11954,
        'crastinal': 11582
    }
    if 'tense' in others:
        tense = others['tense']
        if tense['main'] == 'past':
            avoir_clause = Clause(L(L1886))
            avoir_clause = clause_in.add_inflection(C.present)
            clause_in.attach_left_of_root(avoir_clause, C.auxiliary_verb)
            clause_in.add_inflection(C.past_participle)
        elif tense['main'] == 'present':
            clause_in = clause_in.add_inflection(C.present)
        elif tense['main'] == 'future':
            clause_in = clause_in.add_inflection(C.future)
        clause_in.add_inflection(C.indicative)
        # TODO: put this somewhere else so that it can be placed elsewhere if necessary
        if refine_tense := tense.get('refine', False):
            day_clause = Clause(L(refine_tense_dict[refine_tense]))
            clause_in.attach_leftmost(day_clause, C.time_complement)
    return clause_in

def add_nominal_subject(clause_in, subject_in, **others):
    subject_in = subject_in.add_inflection(C.nominative)
    return clause_in.attach_leftmost(subject_in, C.subject)

def add_nominal_object(clause_in, object_in, **others):
    return clause_in.attach_right_of_root(object_in, C.direct_object)

def generate_speaker(**scope):
    if "number" in scope and scope["number"] == "plural":
        output_clause = Clause(L(9288)) # nous
    else:
        output_clause = Clause(L(9094)) # je
    return output_clause

def generate_listener(**scope):
    if "formality" in scope and scope["formality"] in ["informal", "familiar"] and "number" in scope and scope["number"] == "singular":
        output_clause = Clause(L(9096)) # tu
    else:
        output_clause = Clause(L(9289)) # vous
    return output_clause
