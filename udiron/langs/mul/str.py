from tfsl.languages import langs

import udiron.constants as C
from udiron.langs import StrMethods

def surface_join_spaces(tokens_in):
    return " ".join([entry[0] for entry in tokens_in])


def surface_transform_rmspaces(string_in):
    replacements = [C.COMPOUND_SEP + " " + C.COMPOUND_SEP, " " + C.COMPOUND_SEP, C.COMPOUND_SEP + " "]
    for replacement in replacements:
        string_in = string_in.replace(replacement, "")
    return string_in


def select_form_mul(lexeme_in, inflections_in, config_in):
    current_form = lexeme_in.get_forms(inflections_in)[0].representations[0].text
    if config_in.get("compound", False):
        if "left" in config_in["compound"]:
            current_form = C.COMPOUND_SEP + current_form
        if "right" in config_in["compound"]:
            current_form = current_form + C.COMPOUND_SEP
    return current_form


StrMethods.register_select_form(select_form_mul, langs.mul_)
