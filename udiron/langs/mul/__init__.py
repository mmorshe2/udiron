from tfsl import langs

import udiron.constants as C
from udiron.clause import Clause
from udiron.nonlexunit import NonLexicalUnit

endmark_styles = {
    "period": '.',
    "comma": ',',
    "exclamation": '!',
    "question": '?',
    "ques_ques": '??',
    "ques_excl": '?!',
    "excl_excl": '!!',
    "excl_ques": '!?',
    "danda": '।',
    "double_danda": '॥',
    "arabic_period": '\u06d4',
    "interrobang": '\u203d',
    "ellipsis": '\u2026',
    "colon": ':',
    "semicolon": ';',
    "arabic_question": '\u061f',
    "reversed_question": '\u2e2e',
    "ideographic_period": '\u3002',
    "ideographic_comma": '\u3001',
    "fullwidth_comma": '\uff0c',
    "fullwidth_exclamation": '\uff01',
    "fullwidth_question": '\uff1f',
    "fullwidth_semicolon": '\uff1b',
    "fullwidth_colon": '\uff1a'
}

def add_end_mark(clause, **others):
    if "style" in others:
        endmark = endmark_styles[others["style"]]
    elif "mark" in others:
        endmark = others["mark"]
    else:
        endmark = "."
    config_out = {"compound": ["left"]}
    endmark_clause = Clause(NonLexicalUnit(endmark), language=langs.mul_, config=config_out)
    new_clause = clause.attach_rightmost(endmark_clause, C.punctuation)
    return new_clause


bracket_styles = {
    "straight_double": ('"', '"'),
    "curly_double": ("“", "”"),
    "curly_double_right": ("”", "”"),
    "angle_double": ("«", "»"),
    "straight_single": ("'", "'"),
    "curly_single": ("‘", "’"),
    "curly_single_right": ("’", "’"),
    "angle_single": ("‹", "›"),
    "round": ("(", ")"),
    "curly": ("{", "}"),
    "square": ("[", "]"),
    "angle": ("⟨", "⟩"),
    "low9_single": ("‚", "‘"),
    "low9_double": ("„", "“"),
    "guillemet_single": ("\u2039", "\u203a"),
    "guillemet_single_reversed": ("\u203a", "\u2039"),
    "guillemet_double": ("\u00ab", "\u00bb"),
    "guillemet_double_reversed": ("\u00bb", "\u00ab"),
    "guillemet_double_right": ("\u00bb", "\u00bb"),
    "spanish_question": ("\u00bf", "?"),
    "spanish_exclamation": ("\u00a1", "!"),
    "spanish_excl_ques": ("\u00bf\u00a1", "!?"),
    "spanish_ques_excl": ("\u00a1\u00bf", "?!"),
    "fullwidth_round": ("\uff08", "\uff09"),
    "fullwidth_square": ("\uff3b", "\uff3d"),
    "fullwidth_black_lenticular": ("\u3010", "\u3011"),
    "fullwidth_white_corner": ("\u300e", "\u300f"),
    "fullwidth_corner": ("\u300c", "\u300d"),
    "fullwidth_vertical_angle": ("\ufe41", "\ufe42"),
    "fullwidth_double_angle": ("\u300a", "\u300b"),
    "fullwidth_single_angle": ("\u3008", "\u3009")
}


def add_brackets(clause, **others):
    if 'style' in others:
        left_bracket, right_bracket = bracket_styles[others['style']]
    elif ('left' in others) and ('right' in others):
        left_bracket, right_bracket = others['left'], others['right']
    else:
        left_bracket, right_bracket = '"', '"'
    bracket_left = Clause(NonLexicalUnit(left_bracket), language=langs.mul_, config={"compound": ["left"]})
    bracket_right = Clause(NonLexicalUnit(right_bracket), language=langs.mul_, config={"compound": ["right"]})
    new_clause = clause.attach_leftmost(bracket_left, C.punctuation)
    new_clause = new_clause.attach_rightmost(bracket_right, C.punctuation)
    return new_clause


def full_stop(clause_in, **others):
    return add_end_mark(clause_in, **({"style": "period"} | others))


def danda(clause_in, **others):
    return add_end_mark(clause_in, **({"style": "danda"} | others))


def exclamation_mark(clause_in, **others):
    return add_end_mark(clause_in, **({"style": "exclamation"} | others))


def question_mark(clause_in, **others):
    return add_end_mark(clause_in, **({"style": "question"} | others))


def quotation_marks(clause_in, **others):
    return add_brackets(clause_in, **({"style": "straight_double"} | others))


def parentheses(clause_in, **others):
    return add_brackets(clause_in, **({"style": "round"} | others))
