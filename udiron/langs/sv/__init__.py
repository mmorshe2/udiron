from tfsl import L

from udiron.clause import Clause
from udiron.langs.mul import *
import udiron.constants as C
import udiron.langs.sv.str


def inflect_for_gender(clause_in, gendered_in, **others):
    # grammatical gender
    try:
        clause_in_gender = next(stmt.value.id for stmt in gendered_in.lexeme['P5185'])
    except KeyError:
        clause_in_gender = common_gender_
    clause_in.add_inflection(clause_in_gender)
    return clause_in

def add_adjectival_modifier(clause_in, adj_modifier, **others):
    adj_modifier = inflect_for_gender(adj_modifier, clause_in)
    return clause_in.attach_left_of_root(adj_modifier, C.adj_attribute)


def add_nominal_subject(clause_in, subject_in, **others):
    subject_in = subject_in.add_inflection(C.nominative)
    return clause_in.attach_leftmost(subject_in, C.subject)


def add_copular_subject(clause_in, subject_in, inflections=None, **others):
    vara_clause = Clause(L(43343))
    if inflections is not None:
        vara_clause.add_inflections(inflections)
    else:
        vara_clause.add_inflections([C.active_voice, C.present])
    clause_in = clause_in.attach_leftmost(vara_clause, C.copula)
    return add_nominal_subject(clause_in, subject_in)


def add_noun_count(clause_in, **others):
    if others.get('definite', False):
        clause_in.add_inflection(C.definite)
        if clause_in.has_rel(C.adj_attribute):
            adj = clause_in.get_child_from_rel_left(C.adj_attribute)
            adj[0].add_inflection(C.definite)
            den_clause = Clause(L(47067))
            if others.get('number', False) in ['plural', 'paucal']:
                den_clause.add_inflection(C.plural)
            else:
                den_clause = inflect_for_gender(den_clause, clause_in)
            clause_in.attach_leftmost(den_clause, C.determiner)
        else:
            clause_in.add_inflection(C.definite)
        if others.get('number', False) in ['plural', 'paucal']:
            clause_in.add_inflection(C.plural)
        else:
            clause_in.add_inflection(C.singular)
    else:
        if 'number' in others:
            classifier = Clause(L(54978)) # TODO: add a function to construct numbers
        else:
            classifier = Clause(L(46772))
        classifier = inflect_for_gender(classifier, clause_in)
        clause_in.attach_leftmost(classifier, C.numeral_adjective)
    return clause_in

def make_adjective_superlative(clause_in, **others):
    clause_in = clause_in.add_inflection(C.superlative)
    return clause_in

def mark_as_location(clause_in, **others):
    clause_in = clause_in.attach_leftmost(Clause(L(35761)), C.adposition)
    return clause_in

def add_location_modifier(clause_in, location_modifier, **others):
    return clause_in.attach_right_of_root(location_modifier, C.location_complement)

def inflect_for_tense(clause_in, **others):
    # TODO: adjust add_copular_subject to use this
    # TODO: handle aspect and other distinctions
    refine_tense_dict = {
        'hesternal': 592125,
        'hodiernal': 32240,
        'crastinal': 33587
    }
    if 'tense' in others:
        tense = others['tense']
        if tense['main'] == 'past':
            clause_in = clause_in.add_inflection(C.preterite)
            clause_in = clause_in.remove_inflection(C.infinitive)
        elif tense['main'] == 'present':
            clause_in = clause_in.add_inflection(C.present)
            clause_in = clause_in.remove_inflection(C.infinitive)
        elif tense['main'] == 'future':
            kommer_clause = Clause(L(38134), inflections=[C.present])
            att_clause = Clause(L(35723))
            clause_in.attach_left_of_root(kommer_clause, C.auxiliary_verb)
            clause_in.attach_left_of_root(att_clause, C.infinitive_marker)
            clause_in.add_inflection(C.infinitive)
        # TODO: put this somewhere else so that it can be placed elsewhere if necessary
        if refine_tense := tense.get('refine', False):
            day_clause = mark_as_location(Clause(L(refine_tense_dict[refine_tense])))
            clause_in.attach_rightmost(day_clause, C.time_complement)
    return clause_in

def build_existence_clause(clause_in, **others):
    det_clause = Clause(L(47265))
    finnas_clause = Clause(L(46411))
    finnas_clause = finnas_clause.attach_left_of_root(det_clause, C.expletive)
    finnas_clause = finnas_clause.attach_right_of_root(clause_in, C.subject)
    finnas_clause = inflect_for_tense(finnas_clause, **others)
    return finnas_clause

def negate_clause(clause_in, **others):
    negation_clause = Clause(L(46770))
    if clause_in.has_rel(C.auxiliary_verb):
        clause_in = clause_in.attach_after_rel(C.auxiliary_verb, negation_clause, C.negation)
    else:
        clause_in = clause_in.attach_right_of_root(negation_clause, C.negation)
    return clause_in

def add_nominal_object(clause_in, object_in, **others):
    return clause_in.attach_right_of_root(object_in, C.direct_object)

def generate_speaker(**scope):
    if "number" in scope and scope["number"] == "plural":
        output_clause = Clause(L(46969)) # vi
    else:
        output_clause = Clause(L(47279)) # jag
    return output_clause

def generate_listener(**scope):
    if "formality" in scope and scope["formality"] in ["informal", "familiar"]:
        output_clause = Clause(L(47887)) # du
    else:
        output_clause = Clause(L(35715)) # ni
    return output_clause

def add_thematic_inflections(clause_in, grammatical_feature_reqs):
    return clause_in.add_inflections(grammatical_feature_reqs)
