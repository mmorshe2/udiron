from tfsl import L

from udiron.clause import Clause
from udiron.langs.mul import *
import udiron.constants as C
import udiron.langs.de.str

def add_nominal_subject(clause_in, subject_in, **others):
    subject_in = subject_in.add_inflection(C.nominative)
    if subject_in.has_rel(C.determiner):
        (det_clause, det_rel) = subject_in.get_rel(C.determiner)
        det_clause.add_inflection(C.nominative)
    return clause_in.attach_leftmost(subject_in, C.subject)

def add_copular_subject(clause_in, subject_in, **others):
    if 'tense' in others:
        clause_in = inflect_copula_for_tense(clause_in, **others)
    else:
        sein_clause = Clause(L(1761))
        sein_clause.add_inflections([C.active_voice, C.present, C.third_person])
        clause_in = clause_in.attach_leftmost(sein_clause, C.copula)
    return add_nominal_subject(clause_in, subject_in)

def add_adjectival_modifier(clause_in, adj_modifier, **others):
    adj_modifier = inflect_for_gender(adj_modifier, clause_in)
    return clause_in.attach_left_of_root(adj_modifier, C.adj_attribute)

def add_location_modifier(clause_in, location_modifier, **others):
    return clause_in.attach_right_of_root(location_modifier, C.location_complement)

def inflect_for_gender(clause_in, gendered_in, **others):
    # grammatical gender
    try:
        clause_in_gender = next(stmt.value.id for stmt in gendered_in.lexeme['P5185'])
    except KeyError:
        clause_in_gender = common_gender_
    clause_in.add_inflection(clause_in_gender)
    return clause_in

def mark_as_location(clause_in, **others):
    if(clause_in.lexeme.category == C.proper_noun): # TODO don't probe so much
        try:
            clause_in_gender = next(stmt.value.id for stmt in clause_in.lexeme['P5185'])
        except KeyError:
            clause_in_gender = C.masculine
        if(clause_in_gender in [C.masculine, C.feminine]):
            der_clause = Clause(L(59500), inflections=[C.dative, clause_in_gender]) # TODO: subsume into inflect_for_gender?
            clause_in = clause_in.attach_left_of_root(der_clause, C.determiner)
    clause_in = clause_in.attach_leftmost(Clause(L(6748)), C.adposition)
    return clause_in

def add_noun_count(clause_in, **others):
    if others.get('definite', False):
        clause_in.add_inflection(C.definite)
        try:
            adj = clause_in.get_child_from_rel_left(C.adj_attribute)
            adj[0].add_inflection(C.definite)
        except StopIteration:
            pass
        der_clause = Clause(L(59500))
        if C.plural in clause_in.inflections:
            der_clause.add_inflection(C.plural)
        else:
            der_clause = inflect_for_gender(der_clause, clause_in)
        clause_in.attach_leftmost(der_clause, C.determiner)
    else:
        if 'number' in others:
            classifier = Clause(L(50005)) # TODO: add a function to construct numbers
        else:
            classifier = Clause(L(409194))
        classifier = inflect_for_gender(classifier, clause_in)
        clause_in.attach_leftmost(classifier, C.numeral_adjective)
    return clause_in

# TODO: see if this should go somewhere else
def inflect_for_case(clause, case):
    clause.add_inflection(case)
    if clause.has_rel(C.numeral_adjective):
        numadj_child = clause.get_child_from_rel_left(C.numeral_adjective)
        numadj_child[0].add_inflection(case)
    if clause.has_rel(C.adj_attribute):
        adj_child = clause.get_child_from_rel_left(C.adj_attribute)
        adj_child[0].add_inflection(case)
    return clause

refine_tense_dict = {
    'hesternal': 407664,
    'hodiernal': 248785,
    'crastinal': 407661
}

def inflect_copula_for_tense(clause_in, **others):
    # TODO: handle aspect and other distinctions
    tense = others['tense']
    sein_clause = Clause(L(1761))
    sein_clause.add_inflections([C.active_voice, C.third_person])
    if tense['main'] == 'past':
        sein_clause = sein_clause.add_inflection(C.preterite)
        clause_in = clause_in.attach_leftmost(sein_clause, C.copula)
    elif tense['main'] == 'present':
        sein_clause = sein_clause.add_inflection(C.present)
        clause_in = clause_in.attach_leftmost(sein_clause, C.copula)
    elif tense['main'] == 'future':
        werden_clause = Clause(L(297076), inflections=[C.present, C.active_voice, C.third_person])
        clause_in = clause_in.attach_leftmost(werden_clause, C.copula)
    return clause_in

def add_nominal_object(clause_in, object_in, **others):
    return clause_in.attach_right_of_root(object_in, C.direct_object)

def inflect_for_tense(clause_in, **others):
    # TODO: adjust add_copular_subject to use this
    # TODO: handle aspect and other distinctions
    refine_tense_dict = {
        'hesternal': 407664,
        'hodiernal': 248785,
        'crastinal': 407661
    }

    if 'tense' in others:
        tense = others['tense']
        if tense['main'] == 'past':
            clause_in = clause_in.add_inflection(C.preterite)
            clause_in = shift_verbal_particle(clause_in)
            clause_in.add_inflection(C.third_person)
        elif tense['main'] == 'present':
            clause_in = clause_in.add_inflection(C.present)
            clause_in = shift_verbal_particle(clause_in)
            clause_in.add_inflection(C.third_person)
        elif tense['main'] == 'future':
            werden_clause = Clause(L(297076), inflections=[C.present, C.third_person])
            clause_in.attach_left_of_root(werden_clause, C.auxiliary_verb)
            clause_in.add_inflection(C.infinitive)
            clause_in = shift_verb_to_end(clause_in)

        # TODO: put this somewhere else so that it can be placed elsewhere if necessary
        if refine_tense := tense.get('refine', False):
            day_clause = Clause(L(refine_tense_dict[refine_tense]))
            clause_in = attach_after_verb(clause_in, day_clause, C.time_complement)

    return clause_in

def attach_after_verb(clause_in, adjunct_in, rel):
    if clause_in.has_rel(C.auxiliary_verb):
        return clause_in.attach_after_rel(C.auxiliary_verb, adjunct_in, rel)
    else:
        return clause_in.attach_right_of_root(adjunct_in, rel)

def shift_verb_to_end(clause_in):
    rels = []
    for (clause, rel) in reversed(clause_in.get_right_children()):
        (popped_clause, popped_rel) = clause_in.detach(clause, rel)
        rels.insert(-1, (popped_clause, popped_rel))
    for (clause, rel) in rels:
        clause_in.attach_left_of_root(clause, rel)
    if clause_in.has_rel(C.verbal_particle):
        particle_clause, particle_rel = clause_in.detach_rel(C.verbal_particle)
        clause_in.attach_left_of_root(particle_clause, particle_rel)
    return clause_in

def shift_verbal_particle(clause_in):
    if clause_in.has_rel(C.verbal_particle):
        particle_clause, particle_rel = clause_in.get_rel(C.verbal_particle)
        (popped_clause, popped_rel) = clause_in.detach(particle_clause, particle_rel)
        clause_in.attach_rightmost(popped_clause, popped_rel)
    return clause_in

def pop_if_needed(predicate):
    if predicate.lexeme.haswbstatement(C.instance_of, C.separable_verb):
        predicate = predicate.pop()
        particle_clause, _ = predicate.get_rel(C.verbal_particle)
        particle_clause.config["compound"] = 'right'
    return predicate

def add_empty_subject(clause_in, **others):
    es_clause = Clause(L(248776))
    return clause_in.attach_leftmost(es_clause, C.subject)

def add_indirect_object(clause_in, object_in, **others):
    object_in = object_in.add_inflection(C.dative)
    if object_in.has_rel(C.determiner):
        (det_clause, det_rel) = object_in.get_rel(C.determiner)
        det_clause.add_inflection(C.dative)
    return clause_in.attach_right_of_root(object_in, C.indirect_object)
