from tfsl import langs, LexemeForm

from udiron.langs import StrMethods
from udiron.langs.mul.str import surface_join_spaces, surface_transform_rmspaces
import udiron.constants as C

gender_list = [C.neuter, C.masculine, C.feminine]
is_gender_feature = lambda feat: feat in gender_list
form_without_gender = lambda form: not any(filter(is_gender_feature, form.features))

def develop_form_de_adjective(lexeme_in, inflections_in, config_in):
    if C.superlative in inflections_in:
        obtained_form_list = lexeme_in.get_forms((inflections_in - set(gender_list)) | set([C.superlative]))
        return next(filter(form_without_gender, obtained_form_list))
    elif C.definite in inflections_in:
        obtained_form_list = lexeme_in.get_forms((inflections_in - set(gender_list)) | set([C.positive]))
        return next(filter(form_without_gender, obtained_form_list))
    else:
        obtained_form_list = lexeme_in.get_forms([C.positive, C.attributive])
        return obtained_form_list[0]

def develop_form_de(lexeme_in, inflections_in, config_in):
    if lexeme_in.category == C.adjective:
        obtained_form = develop_form_de_adjective(lexeme_in, inflections_in, config_in)
    elif lexeme_in.category == C.noun:
        obtained_form = lexeme_in.get_forms([C.singular, C.nominative])[0]
    elif lexeme_in.category == C.proper_noun:
        obtained_form = lexeme_in.get_forms([C.nominative])[0]
    else:
        try:
            obtained_form = lexeme_in.get_forms()[0]
        except IndexError:
            obtained_form = LexemeForm("[]" @ langs.de_)
    return obtained_form

def select_form_de(lexeme_in, inflections_in, config_in):
    if len(inflections_in) == 0:
        obtained_form = develop_form_de(lexeme_in, inflections_in, config_in)
    else:
        candidate_forms = lexeme_in.get_forms(inflections_in)
        if len(candidate_forms) == 0:
            obtained_form = develop_form_de(lexeme_in, inflections_in, config_in)
        else:
            obtained_form = candidate_forms[0]
    current_form = next(rep.text for rep in obtained_form.representations if rep.language == langs.de_)
    if "compound" in config_in:
        current_form = handle_compound_de(current_form, config_in["compound"])
    return current_form

def handle_compound_de(current_form, compound_setting):
    if current_form[-1] == '-':
        current_form = current_form[:-1]
    if current_form[0] == '-':
        current_form = current_form[1:]
    if "left" in compound_setting:
        current_form = C.COMPOUND_SEP + current_form
    if "right" in compound_setting:
        current_form = current_form + C.COMPOUND_SEP
    return current_form

def capitalize_de(token):
    capitalized_word = token[0][:1].capitalize() + token[0][1:]
    return (capitalized_word, token[1])

def surface_join_de(tokens_in):
    tokens_in[0] = capitalize_de(tokens_in[0])
    return surface_join_spaces(tokens_in)

def surface_transform_de(string_in):
    return surface_transform_rmspaces(string_in)

StrMethods.register_select_form(select_form_de, langs.de_)
StrMethods.register_surface_join(surface_join_de, langs.de_)
StrMethods.register_surface_transform(surface_transform_de, langs.de_)
