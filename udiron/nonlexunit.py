from tfsl import langs, LexemeForm, LexemeSense


# stuff for punctuation
class NonLexicalUnit:
    def __init__(self, surface, **settings):
        self.surface = surface

        if "category" in settings:
            self.category = settings["category"]
        if "features" in settings:
            self.features = settings["features"]
        if "lang" in settings:
            self.language = settings["lang"]
        else:
            self.language = langs.mul_
        
        self.config = {}

    def get_language(self):
        return self.language

    def get_forms(self, inflections=None):
        representation = self.surface @ self.language
        return [LexemeForm(representation, getattr(self, 'features', []))]

    def get_senses(self):
        return [LexemeSense(self.surface @ self.language)]

    @property
    def lemmata(self):
        return self.surface


def blanknode(**settings):
    return NonLexicalUnit("", **settings)
