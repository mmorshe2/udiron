from udiron.langs import StrMethods


def select_form(lexeme_in, inflections_in, language, config):
    select_form_lang = StrMethods.get_select_form(language)
    return select_form_lang(lexeme_in, inflections_in, config)


def surface_join(tokens_in, language):
    surface_join_lang = StrMethods.get_surface_join(language)
    return surface_join_lang(tokens_in)


def surface_transform(string_in, language):
    surface_transform_lang = StrMethods.get_surface_transform(language)
    return surface_transform_lang(string_in)
