# Udiron
<pre>
A succession of words not amenable to division into metrical feet is called Prose.
Chronicle and Tale are its two varieties. Of these Chronicle, we are told,—
Is what is narrated by the Hero himself exclusively; the other by the Hero as well as by any other person.
The showing forth of one's own merits is not here, in view of his being a recorder of events that have actually occurred, a blemish.
This restriction, however, is not observed, in as much as there [in Ākhyāyikā] also other persons narrate.
That another person narrates or he himself does it—what kind of a ground for distinction is this?
</pre>
—Kāvyādarśa of Daņḍin, i. 23-25 (tr. S. K. Belvalkar)

## About

Udiron
(from the Bengali pronunciation উদীরণ of the Sanskrit [उदीरण](http://www.sanskrit-linguistics.org/dcs/index.php?contents=lemma&IDWord=56551) "communicating, saying, speaking",
 an apocopic form of the Italian *udirono* "they heard",
 and a form of the Ladin for "we will hear")
is a natural language generation system that uses Wikidata lexemes as primitives to construct syntax trees based on the Universal Dependencies (UD) annotation scheme.

(The author of this project is willing to relinquish the name Udiron to the set of renderers that will actually be in use on the Abstract Wikipedia, whether or not derived from what is developed here.)

## Setup

In addition to having the [sortedcontainers](http://www.grantjenks.com/docs/sortedcontainers/) library installed,
you should at least have [tfsl](https://phabricator.wikimedia.org/source/tool-twofivesixlex/) somewhere on your system and should configure your Python however you see fit so that `import tfsl` in your Python works.

Udiron will download lexemes as requested and cache them for later use; make sure that the `udiron_lexeme_path` in `udiron/utils.py` is a writable directory before starting to use it.

## Use

The primary unit of composition is the Clause.
At its core is a lexeme, a sense from that lexeme, some indicators of a desired inflection on that lexeme, and a list of other Clauses which *depend* on this Clause.
These dependencies in effect form a sort of syntax tree, and while the relationships between Clauses in the tree are indicated by UD relations, complete compliance with UD is not a requirement here.
Once a syntax tree is assembled, it suffices to stringify (that is, use the `str()` Python built-in) the root clause to generate text from that tree.

There are a number of levels at which the use of Udiron may take place:

* The lowest level involves *any* direct calls to Clause object methods. (No tests for these are available *yet*.)
* The level just above this involves operating primarily with language-specific primitives which themselves directly manipulate Clause objects. (Tests for Bengali are available in `tests/bn_*.py`.)
* ... (levels above this should be more and more abstract; perhaps references to abstract content can begin at this level?)

Perhaps when this is better developed a more thorough tutorial on its use will come. Here's a [not-quite-as-thorough tutorial](adding_languages.md) on adding languages to Udiron.

More details may be found [here](details.md).

## Licensing

All code herein is Apache 2.0 unless otherwise stated below.
