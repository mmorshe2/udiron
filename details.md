This document describes the current state of sentence processing in Udiron. 

## Clause structure

The Clause is the primary unit of composition in Udiron.
In its simplest form, it serves as a container for a lexeme, but in order to control how that lexeme appears in a resulting string, more than just the lexeme is necessary:

* language: Note that while the Language object used here, if not provided explicitly when constructing the Clause, is obtained from the lexeme, it is entirely appropriate to adjust the language object afterwards.
How the Valencians treat a particular lexeme may be different from how the Barcelonans treat it, and so that difference should be signaled from the start.
* inflections: A set of inflections which should be applied to a lexeme in generating strings.
Note that the set does not need to correspond to the exact set of features on a form of the lexeme, as this set is used with `selectForm()` below.
* sense: The sense is not currently used in any non-language-specific machinery, though it might find use with language-specific machinery.

The syntactic tree structure that a Clause can form a part of originates in a Clause having a parent and optionally any number of child Clauses:

* children: Every Clause directly dependent on this Clause is added as a child. The notion of dependency here is derived from that notion as defined in Universal Dependencies (UD), where the different parts of a constituent attach to its head, and that constituent (through its head) attaches to other constituents' heads until a root is reached.
* parent: When a Clause is created, its parent is the UD root by default. This is only ever changed directly if the Clause is added as a child to another Clause.

While the parent and children are important for structure, the order of children is also important, both relative to the root and relative to its parent:

* index: This value indicates the position of the Clause's lexeme in the string generated at the syntax tree's root. The actual value of the number can itself be neglected in further processing, but its relation to other indices in the tree cannot, and so this is maintained internally throughout the lifetime of the Clause (shifted appropriately if the Clause is added a child to another Clause).
* position: The position is intended for discontinuous constituencies, so that their components can be maintained contiguously in the tree structure but split up in the surface form.
Absent work on handling these, it currently corresponds exactly to the index.

As a nod to the idea of constituency in this dependency-driven environment, the values `range_left` and `range_right` indicate the leftmost and rightmost indices that fall within the constituency rooted at that Clause. These will necessarily shift as children are added to a Clause.

The last part of a Clause is the config, which is intended for language-specific information important for processing a Clause.
It may be possible to migrate an oft-used piece of information out of the config into a separate part of the Clause, but the hope is that this happens rarely.

## Language-specific functions

At the moment the only language-specific machinery exists for Bengali; other such sets of machinery are absolutely welcome.

The following list serves as an example of the sorts of manipulations for which methods were defined or are planned to be defined, and should not constrain your imagination with respect to whatever other kinds of syntactic tree manipulations your language might require:

* Adding case markers to noun Clauses (after resetting any already on them)
* Adding formal/colloquial register markers to Clauses (after resetting any already on them)
* Adding tense/aspect/mood markings to verb Clauses
* Adding a person marking to a verb Clause based on a given subject Clause
* Adding a subject Clause to a verb Clause (optionally changing the inflection from the default for that position)
* Adding an object Clause to a verb Clause (optionally changing the inflection from the default for that position)
* Adding an adverbial/postpositional Clause to a verb Clause
* Adding a postpositional Clause to a noun Clause
* Adding an adjectival Clause to a noun Clause
* Adding a genitive (possessor etc.) Clause to a noun Clause
* Adding punctuation (periods, question/exclamation points, quotation marks, brackets)

## Generating strings

Strings are primarily generated with the `__str__()` method of a Clause, along with the `getTokens()` and `getCurrentForm()` methods.
Within these methods are calls to four language-specific functions, which must be registered with `udiron.langs.StrMethods` before they can be used.

The following must be defined for the language of each Clause in the tree:

* `selectForm()` should take the lexeme and inflections in a Clause and return a form from the lexeme based on those inflections.
(This is also where forms may be directly constructed in the event that a Clause's lexeme turns out to be incomplete.)
* `processConfig()` should take the resulting form and the config in the same Clause and process the resulting form.
(If no extra configuration is given or defined for a language, then it is enough for this method to simply return the form provided.)

After these methods are executed, a list with only a tuple consisting of the resulting form and the lexeme's position is created.

The same methods and list generation are executed for each of the Clause's children and their results used to augment the lists of the parent.

The following must then be defined at least for the language of the Clause at the root of the syntax tree:

* `surfaceJoin()` should take the final list of (form, position) pairs and stitch them into a string.
Other processing which requires knowledge of the separated forms may take place here.
* `surfaceTransform()` should take the resulting string and perform any additional transformations not dependent on knowledge of the separated forms on it.
This is currently where the removal of spaces in compounding takes place.

Registering one of the above methods `method` for a given Language object `lang` amounts to importing `StrMethods` from `udiron.langs` and then running `StrMethods.register[methodname](method, lang)`.
For example, to register Bengali's `selectForm`, the line `StrMethods.registerSelectForm(selectForm_bn, langs.bn_)` was run.
If any of the four methods is not registered, an error will result.
(A language fallback mechanism for these methods is planned to be added.)

An option for a non-parent clause to undergo a `surfaceJoin/surfaceTransform` step before propagation upward, or for a `processConfig` step to take place on the result of such propagation, is yet to be considered.
The pairs might even be worth merging if enough issues with their methods' separation arise.
