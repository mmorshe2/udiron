import unittest

from tfsl.lexeme import L

from udiron import Clause, clause_list_key

def get_key_list(child_list):
    return [clause_list_key(child) for child in child_list]

class TestClauseMethods(unittest.TestCase):
    def setUp(self):
        self.be_clause = Clause(L(1883))
        self.i_clause = Clause(L(487))
        self.tall_clause = Clause(L(1296))
        self.john_clause = Clause(L(238116))
        self.who_clause = Clause(L(56))
        self.play_clause = Clause(L(1292))
        self.like_clause = Clause(L(45034))
        self.water_clause = Clause(L(3302))
        self.trombone_clause = Clause(L(329641))
        self.baseball_clause = Clause(L(30048))
        self.for_clause = Clause(L(2989))
        self.chocolate_clause = Clause(L(7997))

        self.subject_rel = 'Q164573'
        self.subjclause_rel = 'Q19708532'
        self.dirobj_rel = 'Q2990574'
        self.adjattr_rel = 'Q10401368'
        self.reltvzr_rel = 'Q56870226'
        self.relcl_rel = 'Q1402059'
        self.adpos_rel = 'Q134316'
        self.adposp_rel = 'Q1816188'

    def check_tree(self, root, target_left, target_right, left_keys, right_keys, target_indices):
        children_left = root.get_left_children()
        children_right = root.get_right_children()
        self.assertListEqual(children_left, target_left)
        self.assertListEqual(children_right, target_right)
        self.assertEqual(get_key_list(children_left), left_keys)
        self.assertEqual(get_key_list(children_right), right_keys)
        self.assertEqual(root.index_range, target_indices)

    def test_attach_left_of_root_simple_1(self):
        be_clause = self.be_clause
        be_clause.attach_left_of_root(self.i_clause, self.subject_rel)
        target_list = [(self.i_clause, self.subject_rel)]
        self.check_tree(be_clause, target_list, [], [-1], [], (-1, 0))

        be_clause.attach_left_of_root(self.trombone_clause, self.subjclause_rel)
        target_list.append((self.trombone_clause, self.subjclause_rel))
        self.check_tree(be_clause, target_list, [], [-2, -1], [], (-2, 0))

    def test_attach_right_of_root_simple_1(self):
        be_clause = self.be_clause
        be_clause.attach_right_of_root(self.i_clause, self.subject_rel)
        target_list = [(self.i_clause, self.subject_rel)]
        self.check_tree(be_clause, [], target_list, [], [1], (0, 1))

        be_clause.attach_right_of_root(self.trombone_clause, self.subjclause_rel)
        target_list.insert(0, (self.trombone_clause, self.subjclause_rel))
        self.check_tree(be_clause, [], target_list, [], [1, 2], (0, 2))

    def test_attach_left_right_of_root_simple(self):
        be_clause = self.be_clause
        be_clause.attach_left_of_root(self.i_clause, self.subject_rel)
        be_clause.attach_right_of_root(self.tall_clause, self.adjattr_rel)
        target_left = [(self.i_clause, self.subject_rel)]
        target_right = [(self.tall_clause, self.adjattr_rel)]
        self.check_tree(be_clause, target_left, target_right, [-1], [1], (-1, 1))

    def test_attach_left_of_root_simple_2(self):
        be_clause = self.be_clause
        be_clause.attach_left_of_root(self.i_clause, self.subject_rel)
        be_clause.attach_right_of_root(self.tall_clause, self.adjattr_rel)
        be_clause.attach_left_of_root(self.john_clause, self.subjclause_rel)
        target_left = [(self.i_clause, self.subject_rel),
                       (self.john_clause, self.subjclause_rel)]
        target_right = [(self.tall_clause, self.adjattr_rel)]
        self.check_tree(be_clause, target_left, target_right, [-2, -1], [1], (-2, 1))

    def test_attach_right_of_root_simple_2(self):
        be_clause = self.be_clause
        be_clause.attach_left_of_root(self.i_clause, self.subject_rel)
        be_clause.attach_right_of_root(self.tall_clause, self.adjattr_rel)
        be_clause.attach_right_of_root(self.john_clause, self.subjclause_rel)
        target_left = [(self.i_clause, self.subject_rel)]
        target_right = [(self.john_clause, self.subjclause_rel),
                        (self.tall_clause, self.adjattr_rel)]
        self.check_tree(be_clause, target_left, target_right, [-1], [1, 2], (-1, 2))

    def test_attach_left_of_root_simple_3(self):
        water_clause = self.water_clause
        water_clause.attach_left_of_root(self.like_clause, self.adpos_rel)
        chocolate_clause = self.chocolate_clause
        chocolate_clause.attach_right_of_root(self.for_clause, self.adpos_rel)

        be_clause = self.be_clause
        be_clause.attach_left_of_root(chocolate_clause, self.adposp_rel)
        target_left = [(chocolate_clause, self.adposp_rel)]
        target_right = []
        self.check_tree(be_clause, target_left, target_right, [-2], [], (-2, 0))

        be_clause.attach_left_of_root(self.john_clause, self.subjclause_rel)
        target_left = [(chocolate_clause, self.adposp_rel),
                       (self.john_clause, self.subjclause_rel)]
        target_right = []
        self.check_tree(be_clause, target_left, target_right, [-3, -1], [], (-3, 0))

        play_clause = self.play_clause
        play_clause.attach_left_of_root(water_clause, self.adposp_rel)
        target_left = [(water_clause, self.adposp_rel)]
        target_right = []
        self.check_tree(play_clause, target_left, target_right, [-1], [], (-2, 0))

    def test_attach_right_of_root_simple_3(self):
        water_clause = self.water_clause
        water_clause.attach_left_of_root(self.like_clause, self.adpos_rel)
        chocolate_clause = self.chocolate_clause
        chocolate_clause.attach_right_of_root(self.for_clause, self.adpos_rel)

        be_clause = self.be_clause
        be_clause.attach_right_of_root(chocolate_clause, self.adposp_rel)
        target_left = []
        target_right = [(chocolate_clause, self.adposp_rel)]
        self.check_tree(be_clause, target_left, target_right, [], [1], (0, 2))

        be_clause.attach_right_of_root(self.john_clause, self.subjclause_rel)
        target_left = []
        target_right = [(self.john_clause, self.subjclause_rel),
                       (chocolate_clause, self.adposp_rel)]
        self.check_tree(be_clause, target_left, target_right, [], [1, 2], (0, 3))

        play_clause = self.play_clause
        play_clause.attach_right_of_root(water_clause, self.adposp_rel)
        target_left = []
        target_right = [(water_clause, self.adposp_rel)]
        self.check_tree(play_clause, target_left, target_right, [], [2], (0, 2))

    def test_detach_left_of_root_simple(self):
        be_clause = self.be_clause
        be_clause.attach_left_of_root(self.i_clause, self.subject_rel)
        be_clause.attach_right_of_root(self.tall_clause, self.adjattr_rel)
        be_clause.attach_left_of_root(self.john_clause, self.subjclause_rel)
        be_clause.detach(self.john_clause, self.subjclause_rel)

        target_left = [(self.i_clause, self.subject_rel)]
        target_right = [(self.tall_clause, self.adjattr_rel)]
        self.check_tree(be_clause, target_left, target_right, [-1], [1], (-1, 1))

        be_clause.detach(self.i_clause, self.subject_rel)
        target_left = []
        target_right = [(self.tall_clause, self.adjattr_rel)]
        self.check_tree(be_clause, target_left, target_right, [], [1], (0, 1))

    def test_detach_right_of_root_simple(self):
        be_clause = self.be_clause
        be_clause.attach_left_of_root(self.i_clause, self.subject_rel)
        be_clause.attach_right_of_root(self.tall_clause, self.adjattr_rel)
        be_clause.attach_right_of_root(self.john_clause, self.subjclause_rel)
        be_clause.detach(self.john_clause, self.subjclause_rel)
        target_left = [(self.i_clause, self.subject_rel)]
        target_right = [(self.tall_clause, self.adjattr_rel)]
        self.check_tree(be_clause, target_left, target_right, [-1], [1], (-1, 1))

        be_clause.detach(self.tall_clause, self.adjattr_rel)
        target_left = [(self.i_clause, self.subject_rel)]
        target_right = []
        self.check_tree(be_clause, target_left, target_right, [-1], [], (-1, 0))

    def test_attach_leftmost_simple_1(self):
        be_clause = self.be_clause
        be_clause.attach_leftmost(self.i_clause, self.subject_rel)
        target_list = [(self.i_clause, self.subject_rel)]
        self.check_tree(be_clause, target_list, [], [-1], [], (-1, 0))

        be_clause.attach_leftmost(self.trombone_clause, self.subjclause_rel)
        target_list.insert(0, (self.trombone_clause, self.subjclause_rel))
        self.check_tree(be_clause, target_list, [], [-2, -1], [], (-2, 0))

    def test_attach_rightmost_simple_1(self):
        be_clause = self.be_clause
        be_clause.attach_rightmost(self.i_clause, self.subject_rel)
        target_list = [(self.i_clause, self.subject_rel)]
        self.check_tree(be_clause, [], target_list, [], [1], (0, 1))

        be_clause.attach_rightmost(self.trombone_clause, self.subjclause_rel)
        target_list.append((self.trombone_clause, self.subjclause_rel))
        self.check_tree(be_clause, [], target_list, [], [1, 2], (0, 2))

    def test_attach_after_rel_left_simple_1(self):
        be_clause = self.be_clause
        be_clause.attach_left_of_root(self.i_clause, self.subject_rel)
        be_clause.attach_left_of_root(self.john_clause, self.subjclause_rel)
        be_clause.attach_left_of_root(self.trombone_clause, self.dirobj_rel)
        play_clause = self.play_clause
        play_clause.attach_left_of_root(self.who_clause, self.reltvzr_rel)
        be_clause.attach_after_rel(self.subjclause_rel, play_clause, self.relcl_rel)
        target_list = [(self.i_clause, self.subject_rel),
                       (self.john_clause, self.subjclause_rel),
                       (play_clause, self.relcl_rel),
                       (self.trombone_clause, self.dirobj_rel)]
        self.check_tree(be_clause, target_list, [], [-5,-4,-2,-1], [], (-5, 0))

    def test_attach_after_rel_left_simple_2(self):
        be_clause = self.be_clause
        be_clause.attach_left_of_root(self.i_clause, self.subject_rel)
        be_clause.attach_left_of_root(self.john_clause, self.subjclause_rel)
        be_clause.attach_left_of_root(self.trombone_clause, self.dirobj_rel)
        play_clause = self.play_clause
        play_clause.attach_left_of_root(self.who_clause, self.reltvzr_rel)
        be_clause.attach_after_rel(self.dirobj_rel, play_clause, self.relcl_rel)
        target_list = [(self.i_clause, self.subject_rel),
                       (self.john_clause, self.subjclause_rel),
                       (self.trombone_clause, self.dirobj_rel),
                       (play_clause, self.relcl_rel)]
        self.check_tree(be_clause, target_list, [], [-5,-4,-3,-1], [], (-5, 0))

    def test_attach_after_rel_right_simple_1(self):
        be_clause = self.be_clause
        be_clause.attach_right_of_root(self.i_clause, self.subject_rel)
        be_clause.attach_right_of_root(self.john_clause, self.subjclause_rel)
        be_clause.attach_right_of_root(self.trombone_clause, self.dirobj_rel)
        play_clause = self.play_clause
        play_clause.attach_left_of_root(self.who_clause, self.reltvzr_rel)
        be_clause.attach_after_rel(self.dirobj_rel, play_clause, self.relcl_rel)
        target_list = [(self.trombone_clause, self.dirobj_rel),
                       (play_clause, self.relcl_rel),
                       (self.john_clause, self.subjclause_rel),
                       (self.i_clause, self.subject_rel)]
        self.check_tree(be_clause, [], target_list, [], [1,3,4,5], (0, 5))

    def test_attach_after_rel_right_simple_2(self):
        be_clause = self.be_clause
        be_clause.attach_right_of_root(self.i_clause, self.subject_rel)
        be_clause.attach_right_of_root(self.john_clause, self.subjclause_rel)
        be_clause.attach_right_of_root(self.trombone_clause, self.dirobj_rel)
        play_clause = self.play_clause
        play_clause.attach_left_of_root(self.who_clause, self.reltvzr_rel)
        be_clause.attach_after_rel(self.subject_rel, play_clause, self.relcl_rel)
        target_list = [(self.trombone_clause, self.dirobj_rel),
                       (self.john_clause, self.subjclause_rel),
                       (self.i_clause, self.subject_rel),
                       (play_clause, self.relcl_rel)]
        self.check_tree(be_clause, [], target_list, [], [1,2,3,5], (0, 5))

    def test_attach_before_rel_left_simple_1(self):
        be_clause = self.be_clause
        be_clause.attach_left_of_root(self.i_clause, self.subject_rel)
        be_clause.attach_left_of_root(self.john_clause, self.subjclause_rel)
        be_clause.attach_left_of_root(self.trombone_clause, self.dirobj_rel)
        play_clause = self.play_clause
        play_clause.attach_left_of_root(self.who_clause, self.reltvzr_rel)
        be_clause.attach_before_rel(self.subject_rel, play_clause, self.relcl_rel)
        target_list = [(play_clause, self.relcl_rel),
                       (self.i_clause, self.subject_rel),
                       (self.john_clause, self.subjclause_rel),
                       (self.trombone_clause, self.dirobj_rel)]
        self.check_tree(be_clause, target_list, [], [-4,-3,-2,-1], [], (-5, 0))

    def test_attach_before_rel_left_simple_2(self):
        be_clause = self.be_clause
        be_clause.attach_left_of_root(self.i_clause, self.subject_rel)
        be_clause.attach_left_of_root(self.john_clause, self.subjclause_rel)
        be_clause.attach_left_of_root(self.trombone_clause, self.dirobj_rel)
        play_clause = self.play_clause
        play_clause.attach_left_of_root(self.who_clause, self.reltvzr_rel)
        be_clause.attach_after_rel(self.dirobj_rel, play_clause, self.relcl_rel)
        target_list = [(self.i_clause, self.subject_rel),
                       (self.john_clause, self.subjclause_rel),
                       (self.trombone_clause, self.dirobj_rel),
                       (play_clause, self.relcl_rel)]
        self.check_tree(be_clause, target_list, [], [-5,-4,-3,-1], [], (-5, 0))

    def test_attach_before_rel_right_simple_1(self):
        be_clause = self.be_clause
        be_clause.attach_right_of_root(self.i_clause, self.subject_rel)
        be_clause.attach_right_of_root(self.john_clause, self.subjclause_rel)
        be_clause.attach_right_of_root(self.trombone_clause, self.dirobj_rel)
        play_clause = self.play_clause
        play_clause.attach_left_of_root(self.who_clause, self.reltvzr_rel)
        be_clause.attach_before_rel(self.dirobj_rel, play_clause, self.relcl_rel)
        target_list = [(play_clause, self.relcl_rel),
                       (self.trombone_clause, self.dirobj_rel),
                       (self.john_clause, self.subjclause_rel),
                       (self.i_clause, self.subject_rel)]
        self.check_tree(be_clause, [], target_list, [], [2,3,4,5], (0, 5))

    def test_attach_before_rel_right_simple_2(self):
        be_clause = self.be_clause
        be_clause.attach_right_of_root(self.i_clause, self.subject_rel)
        be_clause.attach_right_of_root(self.john_clause, self.subjclause_rel)
        be_clause.attach_right_of_root(self.trombone_clause, self.dirobj_rel)
        play_clause = self.play_clause
        play_clause.attach_left_of_root(self.who_clause, self.reltvzr_rel)
        be_clause.attach_before_rel(self.subjclause_rel, play_clause, self.relcl_rel)
        target_list = [(self.trombone_clause, self.dirobj_rel),
                       (play_clause, self.relcl_rel),
                       (self.john_clause, self.subjclause_rel),
                       (self.i_clause, self.subject_rel)]
        self.check_tree(be_clause, [], target_list, [], [1,3,4,5], (0, 5))

    def test_attach_left_of_root_simple_4(self):
        be_clause = self.be_clause
        be_clause.shift_indices(5)
        be_clause.attach_left_of_root(self.i_clause, self.subject_rel)
        target_list = [(self.i_clause, self.subject_rel)]
        self.check_tree(be_clause, target_list, [], [4], [], (4, 5))

        be_clause.attach_left_of_root(self.trombone_clause, self.subjclause_rel)
        target_list.append((self.trombone_clause, self.subjclause_rel))
        self.check_tree(be_clause, target_list, [], [3, 4], [], (3, 5))

    def test_attach_right_of_root_simple_4(self):
        be_clause = self.be_clause
        be_clause.shift_indices(5)
        be_clause.attach_right_of_root(self.i_clause, self.subject_rel)
        target_list = [(self.i_clause, self.subject_rel)]
        self.check_tree(be_clause, [], target_list, [], [6], (5, 6))

        be_clause.attach_right_of_root(self.trombone_clause, self.subjclause_rel)
        target_list.insert(0, (self.trombone_clause, self.subjclause_rel))
        self.check_tree(be_clause, [], target_list, [], [6, 7], (5, 7))

    def test_attach_leftmost_simple_2(self):
        be_clause = self.be_clause
        be_clause.shift_indices(5)
        be_clause.attach_leftmost(self.i_clause, self.subject_rel)
        target_list = [(self.i_clause, self.subject_rel)]
        self.check_tree(be_clause, target_list, [], [4], [], (4, 5))

        be_clause.attach_leftmost(self.trombone_clause, self.subjclause_rel)
        target_list.insert(0, (self.trombone_clause, self.subjclause_rel))
        self.check_tree(be_clause, target_list, [], [3, 4], [], (3, 5))

    def test_attach_rightmost_simple_2(self):
        be_clause = self.be_clause
        be_clause.shift_indices(5)
        be_clause.attach_rightmost(self.i_clause, self.subject_rel)
        target_list = [(self.i_clause, self.subject_rel)]
        self.check_tree(be_clause, [], target_list, [], [6], (5, 6))

        be_clause.attach_rightmost(self.trombone_clause, self.subjclause_rel)
        target_list.append((self.trombone_clause, self.subjclause_rel))
        self.check_tree(be_clause, [], target_list, [], [6, 7], (5, 7))

    def test_attach_after_rel_right_simple_3(self):
        be_clause = self.be_clause
        be_clause.shift_indices(5)
        be_clause.attach_right_of_root(self.i_clause, self.subject_rel)
        be_clause.attach_right_of_root(self.john_clause, self.subjclause_rel)
        be_clause.attach_right_of_root(self.trombone_clause, self.dirobj_rel)
        play_clause = self.play_clause
        play_clause.attach_left_of_root(self.who_clause, self.reltvzr_rel)
        be_clause.attach_after_rel(self.subject_rel, play_clause, self.relcl_rel)
        target_list = [(self.trombone_clause, self.dirobj_rel),
                       (self.john_clause, self.subjclause_rel),
                       (self.i_clause, self.subject_rel),
                       (play_clause, self.relcl_rel)]
        self.check_tree(be_clause, [], target_list, [], [6,7,8,10], (5, 10))

    def test_attach_before_rel_left_simple_3(self):
        be_clause = self.be_clause
        be_clause.shift_indices(5)
        be_clause.attach_left_of_root(self.i_clause, self.subject_rel)
        be_clause.attach_left_of_root(self.john_clause, self.subjclause_rel)
        be_clause.attach_left_of_root(self.trombone_clause, self.dirobj_rel)
        play_clause = self.play_clause
        play_clause.attach_left_of_root(self.who_clause, self.reltvzr_rel)
        be_clause.attach_before_rel(self.subject_rel, play_clause, self.relcl_rel)
        target_list = [(play_clause, self.relcl_rel),
                       (self.i_clause, self.subject_rel),
                       (self.john_clause, self.subjclause_rel),
                       (self.trombone_clause, self.dirobj_rel)]
        self.check_tree(be_clause, target_list, [], [1,2,3,4], [], (0, 5))

    def test_attach_left_of_root_simple_5(self):
        water_clause = self.water_clause
        water_clause.shift_indices(3)
        water_clause.attach_left_of_root(self.like_clause, self.adpos_rel)

        chocolate_clause = self.chocolate_clause
        chocolate_clause.shift_indices(6)
        chocolate_clause.attach_right_of_root(self.for_clause, self.adpos_rel)

        be_clause = self.be_clause
        be_clause.shift_indices(-4)
        be_clause.attach_left_of_root(chocolate_clause, self.adposp_rel)
        target_left = [(chocolate_clause, self.adposp_rel)]
        target_right = []
        self.check_tree(be_clause, target_left, target_right, [-6], [], (-6, -4))

        be_clause.attach_left_of_root(self.john_clause, self.subjclause_rel)
        target_left = [(chocolate_clause, self.adposp_rel),
                       (self.john_clause, self.subjclause_rel)]
        target_right = []
        self.check_tree(be_clause, target_left, target_right, [-7, -5], [], (-7, -4))

        play_clause = self.play_clause
        play_clause.shift_indices(-6)
        play_clause.attach_left_of_root(water_clause, self.adposp_rel)
        target_left = [(water_clause, self.adposp_rel)]
        target_right = []
        self.check_tree(play_clause, target_left, target_right, [-7], [], (-8, -6))

    def test_attach_right_of_root_simple_5(self):
        water_clause = self.water_clause
        water_clause.shift_indices(3)
        water_clause.attach_left_of_root(self.like_clause, self.adpos_rel)

        chocolate_clause = self.chocolate_clause
        chocolate_clause.shift_indices(6)
        chocolate_clause.attach_right_of_root(self.for_clause, self.adpos_rel)

        be_clause = self.be_clause
        be_clause.shift_indices(-4)
        be_clause.attach_right_of_root(chocolate_clause, self.adposp_rel)
        target_left = []
        target_right = [(chocolate_clause, self.adposp_rel)]
        self.check_tree(be_clause, target_left, target_right, [], [-3], (-4, -2))

        play_clause = self.play_clause
        play_clause.shift_indices(-6)
        play_clause.attach_right_of_root(water_clause, self.adposp_rel)
        target_left = []
        target_right = [(water_clause, self.adposp_rel)]
        self.check_tree(play_clause, target_left, target_right, [], [-4], (-6, -4))
