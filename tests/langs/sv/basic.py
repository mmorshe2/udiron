import unittest

from tfsl.lexeme import L

from udiron import Clause
import udiron.constants as C
import udiron.langs.mul as mul
import udiron.langs.sv as sv

class TestSwedishFunctions(unittest.TestCase):
    def setUp(self):
        self._stor = Clause(L(39168))
        self._planet = Clause(L(47734))
        self._jupiter = Clause(L(584368))

    def test_stor_planet(self):
        planet_clause = self._planet
        planet_clause.add_inflections([C.singular, C.nominative, C.indefinite])
        planet_clause = sv.add_adjectival_modifier(planet_clause, self._stor)
        self.assertEqual(str(planet_clause), "Stor planet")

    def test_jupiter_är_stor(self):
        stor_clause = self._stor
        stor_clause = sv.add_copular_subject(stor_clause, self._jupiter, [C.active_voice, C.present])
        stor_clause = mul.full_stop(stor_clause)
        self.assertEqual(str(stor_clause), "Jupiter är stor.")
