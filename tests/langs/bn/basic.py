import unittest

from tfsl.lexeme import L

from udiron import Clause
import udiron.constants as C
import udiron.langs.bn as bn
import udiron.langs.mul as mul

class TestBengaliFunctions(unittest.TestCase):
    def setUp(self):
        self._বড় = Clause(L(477554))
        self._গ্রহ = Clause(L(310109))
        self._বৃহস্পতি = Clause(L(20409))

    def test_বড়_গ্রহ(self):
        গ্রহ_clause = self._গ্রহ
        গ্রহ_clause = bn.add_adjectival_modifier(গ্রহ_clause, self._বড়)
        গ্রহ_clause.add_inflection(C.nominative)
        self.assertEqual(str(গ্রহ_clause), "বড় গ্রহ")

    def test_বৃহস্পতি_বড়(self):
        বড়_clause = self._বড়
        বড়_clause = bn.add_nominal_subject(বড়_clause, self._বৃহস্পতি)
        বড়_clause = mul.danda(বড়_clause)
        self.assertEqual(str(বড়_clause), "বৃহস্পতি বড়।")
